# cards-wallet

This project was build only for demonstrations purposes.


### Generate a new self signed certificate ###
Run the command above at *"cards-wallet/certs"* path to generate a new self signed certificate inside JKS
```sh
$ keytool -genkey -keyalg RSA -alias cards_wallet_cert -keystore keystore.jks -storepass secret123 -validity 360 -keysize 2048  
```

Fill all the requested information, but attention to the password, because that must match to the jks.secret property inside application.yml.


**NOTE**: keytool is find at <JAVA_HOME>/bin, so if you haven't the keytool configured, you can use this way: 

```sh
$ <PATH TO JAVA HOME>/bin/keytool -genkey -keyalg RSA -alias selfsigned -keystore keystore.jks -storepass password -validity 360 -keysize 2048
```