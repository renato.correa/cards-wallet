package br.com.rc.cards.wallet.repositories;

import br.com.rc.cards.wallet.repositories.types.CardDataType;
import org.springframework.data.repository.CrudRepository;

public interface CardRepository extends CrudRepository<CardDataType, String> {

}
