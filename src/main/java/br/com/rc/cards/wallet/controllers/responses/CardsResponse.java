package br.com.rc.cards.wallet.controllers.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CardsResponse {
    @JsonProperty("card_number")
    private String cardNumber;

    @JsonProperty("expiration_date")
    private String expirationDate;

    @JsonProperty("card_customer_name")
    private String cardCustomerName;

    @JsonProperty("brand")
    private String brand;
}
