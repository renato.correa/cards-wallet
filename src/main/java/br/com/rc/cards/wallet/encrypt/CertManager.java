package br.com.rc.cards.wallet.encrypt;

import org.apache.tomcat.util.http.fileupload.IOUtils;

import javax.crypto.Cipher;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.util.Base64;

import static javax.crypto.Cipher.DECRYPT_MODE;
import static javax.crypto.Cipher.ENCRYPT_MODE;

public class CertManager {
    private String keystoreAlias;

    private String keystorePassword;

    public CertManager(final String keystoreAlias, final String keystorePassword) {
        this.keystoreAlias = keystoreAlias;
        this.keystorePassword = keystorePassword;
    }

    private KeyStore loadJKS() throws GeneralSecurityException, IOException {
        InputStream is = null;
        try {
            is = getClass().getClassLoader().getResourceAsStream("certs/keystore.jks");

            final KeyStore keystore = KeyStore.getInstance("JKS");
            keystore.load(is, keystorePassword.toCharArray());

            return keystore;
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    public String encrypt(final String messageToEncrypt) throws GeneralSecurityException, IOException {
        final KeyStore keystore = loadJKS();
        final Certificate cert = keystore.getCertificate(keystoreAlias);
        final PublicKey publicKey = cert.getPublicKey();


        final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(ENCRYPT_MODE, publicKey);
        final byte[] encryptedBytes = cipher.doFinal(messageToEncrypt.getBytes());
        final String contentEncrypt = Base64.getEncoder().encodeToString(encryptedBytes);

        return contentEncrypt;
    }

    public String decrypt(final String messageToDecrypt) throws GeneralSecurityException, IOException {
        final KeyStore keystore = loadJKS();
        final PrivateKey key = (PrivateKey) keystore.getKey(keystoreAlias, keystorePassword.toCharArray());

        final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(DECRYPT_MODE, key);
        final byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(messageToDecrypt.getBytes()));

        return new String(decryptedBytes);
    }
}