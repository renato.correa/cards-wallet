package br.com.rc.cards.wallet.usecase;

import br.com.rc.cards.wallet.encrypt.CertManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EncryptCardDataUseCase {
    @Value("jks.alias")
    private String keystoreAlias;

    @Value("jks.secret")
    private String keystorePassword;

    private void execute(){
        final CertManager certManager = new CertManager(keystoreAlias, keystorePassword);
    }
}
