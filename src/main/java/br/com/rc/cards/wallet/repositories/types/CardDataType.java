package br.com.rc.cards.wallet.repositories.types;

import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "cards")
@Data
public class CardDataType {

    @Id
    private Long cardId;

    @Column(nullable = false)
    private Long customerId;

    @Column(nullable = false)
    private String cardData;
}
