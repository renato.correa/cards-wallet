package br.com.rc.cards.wallet.usecase;

import br.com.rc.cards.wallet.repositories.CardRepository;
import br.com.rc.cards.wallet.repositories.types.CardDataType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class GetCustomerCardsUseCase {

    private CardRepository cardRepository;

    public List<CardDataType> execute(){
        final List<CardDataType> cardList = new ArrayList<>();
        final Iterable<CardDataType> iterator = cardRepository.findAll();
        iterator.forEach(cardList::add);

        return cardList;
    }
}
