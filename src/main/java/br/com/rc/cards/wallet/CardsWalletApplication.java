package br.com.rc.cards.wallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardsWalletApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardsWalletApplication.class, args);
	}

}
