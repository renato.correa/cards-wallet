package br.com.rc.cards.wallet.controllers;

import br.com.rc.cards.wallet.repositories.types.CardDataType;
import br.com.rc.cards.wallet.usecase.GetCustomerCardsUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CardsController {

    @Autowired
    private GetCustomerCardsUseCase getCustomerCardsUseCase;

    @GetMapping("/cards")
    public List<CardDataType> getCards() {
        /*return CardsResponse.builder()
                .brand("Mastercard")
                .cardCustomerName("Renato Correa")
                .cardNumber("xxx.xxx.xxx.5643")
                .expirationDate("05/2018")
                .build();*/
        return getCustomerCardsUseCase.execute();
    }
}
